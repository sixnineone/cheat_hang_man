import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class CheatHangMan {

	static int steps; // number of steps
	static int wordLength;
	static ArrayList<String> wordLib = new ArrayList<String>();
	static ArrayList<Character> usedLetters = new ArrayList<Character>();
	static char[] curWord;
	static ArrayList<Character> correctUsedLetters = new ArrayList<Character>();
	static final int numStep = 15; // setting the number of steps

	public static void main(String[] args) throws FileNotFoundException {

		Scanner in = new Scanner(System.in);

		// Welcome panel
		System.out.println("Welcome to HangMan Game!");
		System.out.println("________________________");

		// Generate the length of the word
		wordLength = 2 + (int) (Math.random() * (8));
		getDictionary();
		System.out.println("| The word's length is " + wordLength + "|");
		System.out.println("-------------------------");
		curWord = new char[wordLength];
		printNow();

		// Main process of the program
		// if the user exceeds the number of steps,
		// or the user lose, the game will end.
		while (gameState() == false && steps < numStep) {

			System.out.println("Please guess a word!");

			char guess = ' ';

			// This array list includes all the used letters
			ArrayList<Character> allUsedLetters = new ArrayList<Character>();
			for (char c : usedLetters) {
				allUsedLetters.add(c);
			}
			for (char c : correctUsedLetters) {
				allUsedLetters.add(c);
			}

			// check if the enter is valid
			boolean used = true;
			while (used) {

				// get the letter from the user
				guess = in.nextLine().charAt(0);

				// if the letter is already used, find = true

				boolean find = false;
				for (char c : allUsedLetters) {
					if (c == guess)
						find = true;
				}
				used = find;

				// The user will need to type again if find is true
				if (find) {
					System.out.println("You have tried the letter...Type again");
				}

				if (!(guess >= 97 && guess <= 122)) {
					System.out.println("Wrong input! type again!");
					used = true;
				}
			}

			// manipulate the input
			process(guess);

		}

		// determine whether the user has won or not
		if (gameState() == true) {
			System.out.println("_______________________");
			System.out.println("You win! Unbelieveable! ");
		} else {
			System.out.println("_______________________");

			System.out.println("At least you tried. Hahaha");

			// show one of the correct words
			System.out.println("The correct word is:" + wordLib.get((int) (Math.random() * wordLib.size())));
		}

		// end of the game
		System.out.println("_______________________");
		System.out.println("Bye!");

	}

	/*
	 * This method identifies whether the game has end or not by the correctness if
	 * the curWord is full of characters the method will @return true else @return
	 * false
	 */
	public static boolean gameState() {

		// if any char in the array is empty, return false
		for (char c : curWord) {
			if (c == '\0')
				return false;
		}

		// if the method is still not returned, return true
		return true;
	}

	/*
	 * This method will print out the current situation the method will first print
	 * out the correct part of the word The it will print out the incorrect letters
	 * Then it will print the steps left of the user no returned value
	 */
	public static void printNow() {

		System.out.println("_______________________");

		// print the correct part of the word
		System.out.print("Now the word is: ");
		for (char c : curWord) {
			if (c == '\0') {
				// incorrect part will be printed as dash
				System.out.print("_" + " ");
			} else {
				// correct letter will be printed
				System.out.print(c + " ");
			}
		}

		System.out.println();
		System.out.println("_______________________");

		// print the wrong letters
		System.out.print("The incorrect letter(s) so far is(are):");
		for (char c : usedLetters) {
			System.out.print(c + " ");
		}
		System.out.println();

		// print the left steps
		System.out.println("You have " + (numStep - steps) + " step(s) remaining");

	}

	/*
	 * This method will manipulate the input letter. The process will take the
	 * letter to see whether to delete those words, or to acknowledge that the user
	 * is correct
	 */

	public static void process(char guess) {

		// get a copy from word Lib
		ArrayList<String> copy = new ArrayList<String>();
		for (String s : wordLib) {
			copy.add(s);
		}

		for (int i = 0; i < copy.size(); i++) { // check if any string in the arraylist has the letter
			for (int j = 0; j < copy.get(i).length(); j++) {
				if (guess == copy.get(i).charAt(j)) {
					copy.remove(i); // remove the strings with the letter
					i--;
					break;
				}
			}
		}

		// if there are no more letters, that means the program have to acknowledge that
		// the user is correct
		if (copy.size() == 0) {
			noWay(guess);
			// use noWay method
		} else { // some strings can be deleted
			for (int i = 0; i < wordLib.size(); i++) { // check if any string in the array list has the letter
				for (int j = 0; j < wordLib.get(i).length(); j++) {
					if (guess == wordLib.get(i).charAt(j)) {
						wordLib.remove(i); // remove the strings with the letter
						i--;
						break;
					}
				}
			}

			// the correspond words in word Lib have been removed

			System.out.println("This letter is not included!");
			usedLetters.add(guess);
			steps++;

			printNow();

		}

	}

	/*
	 * This method provides the process when the program cannot delete more words
	 */
	public static void noWay(char guess) {

		// choose a random word in the wordLib
		String choose = wordLib.get((int) (Math.random() * wordLib.size()));

		// put the correct letter in the curword
		for (int i = 0; i < choose.length(); i++) {
			if (choose.charAt(i) == guess) {
				curWord[i] = guess;
			}
		}

		// identify which of the index has the guess letter
		// store in the array List
		ArrayList<Integer> indexes = new ArrayList<Integer>();
		for (int i = 0; i < wordLength; i++) {
			if (choose.charAt(i) == guess) {
				indexes.add(i);
			}
		}

		// remove the word in wordLib that does not have the correct letter in the index
		// places
		for (int i = 0; i < wordLib.size(); i++) {
			String s = wordLib.get(i);

			for (int j = 0; j < wordLength; j++) {
				if (indexes.contains(j)) {
					if (s.charAt(j) != guess) {
						wordLib.remove(i);
						i--;
						break;
					}
				} else {
					if (s.charAt(j) == guess) {
						wordLib.remove(i);
						i--;
						break;
					}
				}
			}
		}

		// add that letter to the CUL
		correctUsedLetters.add(guess);

		System.out.println("Good for you!");
		// steps not added
		printNow();
	}

	/*
	 * This method get the first word lib word lib will be filled with words that
	 * have length=wordLength
	 */
	public static void getDictionary() throws FileNotFoundException {
		String filenameRead = "wordLib.txt"; // name of file to read
		File readFile = new File(filenameRead); // create file handler
		Scanner sc = new Scanner(readFile); // Scanner reads the file
		while (sc.hasNextLine()) {
			String s = sc.nextLine();
			if (s.length() == wordLength)
				wordLib.add(s);
		}

	}

}
